#include "libProsopon.h"

// libProsopon lib;

// Constructor
libProsopon::libProsopon(){

}

//Destructors
libProsopon::~libProsopon()
{

}

/**
 FunÁ„o que pega a altura e a largura da imagem e verifica se a quantidade de linhas e colunas est· acima de um determinado limiar.

 \param [in] img: imagem.
 \param [in] limDim: limiar.
 \return True se a imagem possui altura e largura adequadas.
 */
bool libProsopon::funcDimensoes(const Mat img, int limDim){

    if (img.rows > limDim && img.cols > limDim) return true;

    return false;
}

/**
 FunÁ„o que verifica se a imagem È colorida.

 \param [in] img: imagem.
 \return True se a imagem È colorida.
 */
bool libProsopon::funcColor(const Mat img){

    if (img.channels() != 3) return false;

    return true;
}

/**
 FunÁ„o que verifica se o arquivo aberto È uma imagem.

 \param [in] img: imagem.
 \return True se o arquivo È imagem.
 */
bool libProsopon::funcVerificaImg(Mat img){

    if (!img.data) return false;

    return true;

}


/**
 FunÁ„o que pega a largura e a altura da imagem e verifica se a raz„o entre essas duas medidas est· adequada (formato retrato).

 \param [in] img: imagem.
 \param [in] minRaz: raz„o mÌnima.
 \param [in] maxRaz: raz„o m·xima.
 \return True se a imagem possui raz„o adequada.
 */
bool libProsopon::funcRazao(const Mat img, float minRaz, float maxRaz)
{
    // A imagem n„o pode estar no formato paisagem. Logo, maxRaz = 1.0
    // A imagem tambÈm n„o pode ser muito estreita verticalmente. Sugest„o, minRaz = 0.7

    float ratio = (float)img.cols / img.rows;

    if ((ratio >= minRaz) && (ratio <= maxRaz)) return true;

    return false;
}

/**
 // FunÁ„o que verifica a raz„o entre a altura da face e a altura da imagem.

 \param [in] img: imagem.
 \param [in] faceRect: ret‚ngulo da face.
 \param [in] minRaz: raz„o mÌnima.
 \param [in] maxRaz: raz„o m·xima.
 \return True se a raz„o estiver adequada.
 */
int libProsopon::funcAlturaRelat(Mat img, Rect faceRect, float minRaz, float maxRaz){

    float razao = 100 * ((float)(faceRect.height)) / img.rows;

    if (razao < minRaz) {
        return -1;
    } else if (razao > maxRaz) {
        return 1;
    } else {
        return 0;
    }
}


/**
 // FunÁ„o que verifica se o tamanho em bytes da imagem est· entre os limites estabelecidos.

 \param [in] img: imagem.
 \param [in] tamBytes: tamanho da imagem em bytes.
 \param [in] minTam: tamanho mÌnimo.
 \param [in] maxTam: tamanho m·ximo.
 \return True se a raz„o estiver adequada.
 */
bool libProsopon::funcTamanhoBytes(Mat img, long double tamBytes, long double minTam, long double maxTam){

    if (tamBytes >= minTam && tamBytes <= maxTam) return true;

    return false;

}

/**
 // FunÁ„o que detecta faces.

 \param [in] img: imagem.
 \param [out] Faces: vetor de ret‚ngulos com todas as faces detectadas.
 \return True se apenas uma ˙nica face for detectada.
 */
bool libProsopon::funcFaces(Mat img, vector<Rect> &Faces, CascadeClassifier faceCascade){

    faceCascade.detectMultiScale(img, Faces, 1.2);

    if (Faces.size() == 1) return true;

    return false;
}

/**
 // FunÁ„o que detecta olhos em imagem com face.

 \param [in] img: imagem.
 \param [in] Face: regi„o de interesse contendo apenas a face.
 \param [out] Olhos: vetor de ret‚ngulos com todos os olhos detectados.
 \param [in] faceCascade: classificador de face.
 \return True se apenas dois olhos foram detectados.
 */
bool libProsopon::funcOlhosComFace(Mat img, Rect Face, vector<Rect> &Olhos, CascadeClassifier eyeCascade){

    Mat imgRoi(img, Face);

    eyeCascade.detectMultiScale(imgRoi, Olhos,1.2);

    if (Olhos.size() == 2){
        Rect olho1 = Olhos.at(0);
        Rect olho2 = Olhos.at(1);
        olho1.x += Face.x;
        olho2.x += Face.x;
        olho1.y += Face.y;
        olho2.y += Face.y;
        Olhos.at(0) = olho1;
        Olhos.at(1) = olho2;
        return true;
    }
    return false;


}

/**
 // FunÁ„o que detecta olhos em imagem sem face.

 \param [in] img: imagem.
 \param [in] Face: regi„o de interesse contendo apenas a face.
 \param [in] faceCascade: classificador de face.
 \param [out] Olhos: vetor de ret‚ngulos com todos os olhos detectados.
 \return True se apenas dois olhos foram detectados.
 */
bool libProsopon::funcOlhosSemFace(Mat img, vector<Rect> &Olhos, CascadeClassifier eyeCascade){

    //eyeCascade.detectMultiScale(img, Olhos, 1.1, 3, 0, cvSize(10, 10), cvSize(40, 40));
    eyeCascade.detectMultiScale(img, Olhos, 1.1);

    if (Olhos.size() == 2){
        return true;
    }
    return false;

}

bool libProsopon::funcEyesDistance(Mat img, vector<Rect> &Olhos, vector<Rect> &eyeDistance, int maximumDistance)
{
    eyeDistance.clear();
    Rect olho1 = Olhos.at(0);
    Rect olho2 = Olhos.at(1);
    Point olho1Center = Point((olho1.x + olho1.width/2),(olho1.y + olho1.height/2));
    Point olho2Center = Point((olho2.x + olho2.width/2),(olho2.y + olho2.height/2));
    Point diff = olho1Center - olho2Center;
    float distance =  cv::sqrt(diff.x*diff.x + diff.y*diff.y);
    if (distance<maximumDistance)
    {
        eyeDistance.push_back(cv::Rect(olho1Center,olho2Center));
        return true;
    }
    return false;
}

bool libProsopon::eyesHeightValid(Mat img, vector<cv::Rect> &Olhos, int minimumHeight, int maximumHeight) {
    Rect olho1 = Olhos.at(0);
    Rect olho2 = Olhos.at(1);
    if (olho1.height>minimumHeight && olho1.height<maximumHeight && olho2.height>minimumHeight && olho2.height<maximumHeight) {
        return true;
    } else {
        return false;
    }
}

bool libProsopon::funcEyesAlignment(Mat img, vector<cv::Rect> &eyeDistance, vector<cv::Rect> &Olhos) {
    int maximumHeight = (Olhos.at(0).height > Olhos.at(1).height) ? Olhos.at(0).height : Olhos.at(1).height;
    maximumHeight = maximumHeight/2;
    if (eyeDistance.front().height < maximumHeight) {
        return true;
    } else {
        return false;
    }
}

bool libProsopon::eyesToTopValid(Mat img, vector<cv::Rect> &Olhos) {
    Rect olho1 = Olhos.at(0);
    Rect olho2 = Olhos.at(1);
    float center = (olho1.height/2 + olho1.y + olho2.height/2 + olho2.y)/2;
    float rows = img.rows;
    float minimumDistance = 13.0/45.0 * rows;
    float maximumDistance = 23.0/45.0 * rows;
    if (center>minimumDistance && center<maximumDistance) {
        return true;
    } else {
        return false;
    }
}

/**
 // FunÁ„o calcula a quantidade de bytes de uma imagem.

 \param [in] img: imagem.
 \return  A quantidade de bytes de img.
 */
long double libProsopon::funcCalcBytes(Mat img)
{
    // Mat::elemSize() retorna o tamanho de um elemento da matriz em bytes.
    // Mat::total() o n˙mero de elementos da matriz.
    return img.total() * img.elemSize();
}

/**
 // FunÁ„o que estima a posiÁ„o da cabeÁa a partir da posiÁ„o dos olhos.

 \param [in] img: imagem.
 \param [in] Olhos: vetor de ret‚ngulos com todos os olhos detectados.
 \return  Um ret‚ngulo com a posiÁ„o estimada da cabeÁa.
 */
Rect libProsopon::funcEstimaHead(Mat img, vector<Rect> Olhos)
{

    float DistEntreZoios;

    // Estrutura da cabeÁa
    Rect Cabessauro;

    // Olhos esquerdo e direito
    Rect eyeE, eyeD;

    // Just Look at me!
    //
    // Verifica qual olho ocorre primeiro
    //       _....,_           _,...._
    //   _.-` _,..,_'.       .'_,..,_ `-._
    //    _,-`/eyeE\ '.     .' /eyeD\`-,_
    //     '-.\____/.-`     `-.\____/.-'
    //
    //   		     |     |
    //               |     |
    //               /     \
    //              (__   __)
    //              '--' '--'

    if (Olhos[0].x < Olhos[1].x){
        eyeE = Olhos[0];
        eyeD = Olhos[1];
    }
    else{
        eyeE = Olhos[1];
        eyeD = Olhos[0];
    }

    //                                         DistEntreZoios
    //  Dist‚ncia horizontal entre os olhos -  O|----------|O
    DistEntreZoios = abs((eyeE.x + eyeE.width) - eyeD.x);

    // Estima a posiÁ„o da cabeÁa
    float centro;
    Cabessauro.width = 3*DistEntreZoios + eyeE.width + eyeD.width;
    centro = (eyeE.x + (eyeE.width / 2) + eyeD.x + (eyeD.width / 2)) / 2;
    Cabessauro.x = centro - Cabessauro.width / 2;
    Cabessauro.height = 1.5 * Cabessauro.width;
    Cabessauro.y = (eyeE.y + (eyeE.height / 2)) - (Cabessauro.height/2);

    if (Cabessauro.x<0)
    {
        Cabessauro.x = 0;
    }
    if (Cabessauro.y<0)
    {
        Cabessauro.y = 0;
    }

    if (Cabessauro.x + Cabessauro.width >= img.cols)
    {
        Cabessauro.width = img.cols - Cabessauro.x - 1;
    }
    if (Cabessauro.y + Cabessauro.height >= img.rows)
    {
        Cabessauro.height = img.rows - Cabessauro.y - 1;
    }

    return Cabessauro;

}

/**
 // FunÁ„o que verifica se o fundo È ruidoso

 \param [in] img: imagem.
 \param [in] face: ret‚ngulo que contem a face.
 \param [in] varMax: valor a partir do qual a vari‚ncia local È considerada como alta.
 \param [in] perVarMax: mÈdia poderada das vari‚ncias que est„o acima de varMax.
 \param [out] perVarMax: imagem para debug.
 \return  True se o fundo È ruidodo.
 */
bool libProsopon::funcFundo(Mat img, Rect face, float varMax, float perVarMax, Mat &foto_marcada){

    // Utilizados para guardar os valores das mÈdias e vari‚ncias locais
    double media = 0;
    double variancia = 0;

    // Conta a quantidade de elementos na ·rea de interesse
    unsigned int conta = 0;

    // Vers„o gray de foto
    Mat foto_gray;
    if (img.channels() != 1)
        cvtColor(img, foto_gray, CV_BGR2GRAY);
    else
        foto_gray = img.clone();

    // Imagem para debug - Marca as regiıes a serem processadas
    //foto_marcada = img.clone();

    // CÛpia da imagem em float para c·lculo da mÈdia e da vari‚ncia
    Mat foto_float;

    // Matriz que armazena as mÈdias locais
    Mat foto_media = Mat::zeros(img.rows, img.cols, CV_32F);  //  E[X]
    Mat foto_2media = Mat::zeros(img.rows, img.cols, CV_32F); //  E[X^2]
    Mat foto_media2 = Mat::zeros(img.rows, img.cols, CV_32F); //  E[X]^2

    // Matriz que armazena as vari‚ncias locais - E[X^2]-E[X]^2
    Mat foto_variance = Mat::zeros(img.rows, img.cols, CV_32F);

    // Acrescimo na altura da cabeÁa para se chegar ao ombro
    float delta_cabeca = 0.15f;

    // Faz uma cÛpia da imagem para valores float
    foto_gray.convertTo(foto_float, CV_32F);

    // Limite da cabeÁa
    int x, w, y, h;

    //Centro da face
    int xcentro = img.cols / 2;

    x = face.x - delta_cabeca * face.width;
    if (x < 0)  x = 1;

    w = x + face.width + 2 * delta_cabeca * face.width;
    if (w > img.cols)  w = img.cols - 1;

    y = face.y - delta_cabeca*face.height;          // Linha inicial
    if (y < 0) y = 1;

    h = y + face.height + 2 * delta_cabeca*face.height; // Linha final
    if (h>img.rows)
        h = img.rows - 1;

    // Desenha um ret‚ngulo ao redor
    rectangle(foto_marcada,
              Point(x, y),
              Point(w, h),
              Scalar(255, 0, 255),
              2, 8, 0);


    for (int k = 0; k < img.rows; k++){                   // Varia as linhas
        for (int j = 0; j < img.cols; j++){               // Varia as colunas
            if (!((j >= x && j < w && k >= y &&  k < h) ||
                  (k >= h) ||
                  (k <= y && j >= x && j <= w))) {

                media += foto_float.at<float>(k, j);
                conta++;
            }
        }
    }
    media /= conta;

    for (int k = 0; k < img.rows; k++){                   // Varia as linhas
        for (int j = 0; j < img.cols; j++){               // Varia as colunas
            if (!((j >= x && j < w && k >= y &&  k < h) ||
                  (k >= h) ||
                  (k <= y && j >= x && j <= w))) {
                variancia += pow(foto_float.at<float>(k, j) - media, 2);
//                foto_marcada.at<Vec3b>(k, j)[0] = 0;   //B
//                foto_marcada.at<Vec3b>(k, j)[1] = 0;   //G
//                foto_marcada.at<Vec3b>(k, j)[2] = 0;   //R
            }
        }
    }
    variancia /= (conta - 1);

    // Calcula a vari‚ncia => blur(img^2) - blur(img)^2
    blur(foto_float.mul(foto_float), foto_2media, Size(3, 3));
    blur(foto_float, foto_media, Size(3, 3));
    foto_media2 = foto_media.mul(foto_media);

    for (int k = 0; k < foto_float.rows; k++)
        for (int j = 0; j < foto_float.cols; j++)
            foto_variance.at<float>(k, j) = foto_2media.at<float>(k, j) - foto_media2.at<float>(k, j);

    //foto_variance.convertTo(foto_variance, CV_8UC1);

    float alta_variancia = 0;
    float ruido = 0;

    // Marca pixels contidos em regiıes de alta vari‚ncia
    for (int k = 1; k < img.rows - 1; k++){                   // Varia as linhas
        for (int j = 1; j < img.cols - 1; j++){               // Varia as colunas
            if (!((j >= x && j < w && k >= y &&  k < h) ||
                  (k >= h) ||
                  (k <= y && j >= x && j <= w))) {

                if (foto_variance.at<float>(k, j) > varMax){

                    alta_variancia = alta_variancia + 1;

//                    foto_marcada.at<Vec3b>(k, j)[0] = 0;     //B
//                    foto_marcada.at<Vec3b>(k, j)[1] = 0;     //G
//                    foto_marcada.at<Vec3b>(k, j)[2] = 255;   //R

                    // A ocorrÍncia de altas vari‚ncias mais distantes do centro da imagem
                    // ter„o peso maior, pois provavelmente n„o s„o cabelo.
                    ruido = ruido + (float)abs(xcentro - j) / xcentro;
                }
            }
        }
    }

    float perVarCalc = 100.0 * ruido / conta;

    //cout<<endl<<"PerVarCalc = "<<_perVarCalc<<endl<<endl;
    if (perVarCalc > perVarMax)
    {
        return false;
    }
    return true;
}


/**
 // FunÁ„o que converte uma matriz pra uma imagem gray.

 \param [in] mat: matriz de entrada.
 \return  imgOut: imagem de saÌda.
 */
Mat libProsopon::mat2gray(const Mat& mat)
{
    Mat imgOut;
    normalize(mat, imgOut, 0.0, 1.0, NORM_MINMAX);
    return imgOut;
}

/**
 // FunÁ„o que verifica se a face est· centralizada

 \param [in] img: imagem original.
 \param [in] face: ret‚ngulo que contÈm a face.
 \param [in] minDist: mÌnima dist‚ncia percentual permitida.
 \return  True se a face est· centralizada.
 */
bool libProsopon::funcCentro(const Mat& img, Rect face, float minDist)
{
    Point center;

    center.x = face.width/2 + face.x;
    int imgCenter = img.cols/2;

    // Percentagem de afastamento do centro com relaÁ„o ‡ dist‚ncia do centro a um dos cantos da imagem.
    // Se a face est· perfeitamente centralizada, o afastamento È 0. Se o centro da imagem est· localizado
    // em um dos cantos da imagem, o afastamento È 1.
    int absoluteDiff = abs(center.x - imgCenter);
    float Dist = (100 * absoluteDiff)/center.x;

    if (Dist < minDist)	return true;

    return false;
}

/**
 // FunÁ„o que desenha uma linha do centor da imagem para o centro da face.

 \param [in] face: ret‚ngulo que contÈm a face.
 \param [out] img: imagem com a reta desenhada.
 */
void libProsopon::funcRetaCentro(Mat& img, Rect face, float minDist)
{
    double scale = 1;
    Point center;

    center.x = cvRound((face.x + face.width*0.5)*scale);
    center.y = cvRound((face.y + face.height*0.5)*scale);

    float Dist = sqrt(pow(img.rows / 2 , 2) + pow(img.cols / 2 , 2));

    arrowedLine(img, Point(img.cols / 2, img.rows/2), Point(center.x, center.y), Scalar(0,255,0), 8);

    // Desenha tambÈm um circula que define a regi„o dentro da qual o deslocamento e permitido
    circle(img, Point(img.cols / 2, img.rows / 2), Dist*minDist/100, Scalar(255, 255, 255), 8);


}


/**
 // FunÁ„o que verifica se a imagem est· borrada.

 \param [out] img: imagem de entrada.
 \return  True se a face n„o est· borrada.
 */
bool libProsopon::funcVarOfLapl(const Mat& img, float limiteVOfL)
{

    //Analysis of focus measure operators for shape-from-focus
    //Said Pertuz
    //Domenec Puig
    //Miguel Angel Garcia
    //www.elsevier.com/locate/pr

    Mat lap;
    Laplacian(img, lap, CV_64F);

    Scalar mu, sigma;
    meanStdDev(lap, mu, sigma);

    double focusMeasure = sigma.val[0] * sigma.val[0];

    // True se imagem n„o est· borrada
    if (focusMeasure > 10) return true;

    return false;
}

/**
 // FunÁ„o que verifica se o constrate entre o fundo e a face est· adequado

 \param [in] img: imagem de entrada.
 \param [in] face: ret‚ngulo da face.
 \param [in] maxDist: dist‚ncia entre fundo e face a partir da qual o constraste È considerado insuficiente.
 \return  True se on contraste est· bom.
 */
bool libProsopon::funcContraste(Mat img, Rect face, float maxDist)
{
    double mediaFundo = 0.0, mediaFace = 0.0;

    unsigned int contPxFundo = 0, contPxFace = 0;

    Vec3b valPixel;

    // Vers„o gray de foto
    Mat imgGray;
    if (img.channels() != 2)
        cvtColor(img, imgGray, CV_BGR2GRAY);
    else
        imgGray = img.clone();

    // Imagem para debug - Marca as regiıes a serem processadas
    Mat foto_marcada = img.clone();

    // CÛpia da imagem em float para c·lculo da mÈdia e da vari‚ncia
    Mat foto_float;

    // Acrescimo na altura da cabeÁa para se chegar ao ombro
    float delta_cabeca = 0.15f;

    // Faz uma cÛpia da imagem para valores float
    imgGray.convertTo(foto_float, CV_32F);

    // Limite da cabeÁa
    int x, w, y, h;

    //   int xcentro = img_.cols / 2;

    x = face.x - 0.4*delta_cabeca * face.width;
    if (x < 0)  x = 1;

    w = x + face.width + 0.8 * delta_cabeca * face.width;
    if (w > img.cols)  w = img.cols - 1;

    y = face.y - 1.5*delta_cabeca*face.height;          // Linha inicial
    if (y < 0) y = 1;

    h = y + face.height + 3 * delta_cabeca*face.height; // Linha final
    if (h>img.rows) h = img.rows - 1;


    for (int k = 0; k < img.rows; k++){                   // Varia as linhas
        for (int j = 0; j < img.cols; j++){               // Varia as colunas

            if ((j>x && j<w) && (k>y && k<h))
            {
                mediaFace += foto_float.at<float>(k, j);
                contPxFace++;
                //Debug
                valPixel[0] = 0;   //B
                valPixel[1] = 0;   //G
                valPixel[2] = 255; //R
                foto_marcada.at<Vec3b>(k, j) = valPixel;
            }
            else if (k<h){
                mediaFundo += foto_float.at<float>(k, j);
                contPxFundo++;
                //Debug
                valPixel[0] = 255;   //B
                valPixel[1] = 0;   //G
                valPixel[2] = 0; //R
                foto_marcada.at<Vec3b>(k, j) = valPixel;
            }

        }
    }
    mediaFundo /= contPxFundo;
    mediaFace /= contPxFace;

    float valCont = abs(mediaFundo - mediaFace);

    if (valCont > maxDist)
    {
        return true;
    }
    return false;
}


/**
 // FunÁ„o que verifica se o fundo È claro (branco).

 \param [in] img: imagem de entrada.
 \param [in] face: ret‚ngulo da face.
 \param [in] limiar: limiar de branco.
 */
bool libProsopon::funcFundoClaro(Mat img, cv::Rect face, float maxSaturation, float minSaturation, bool faceEstimada)
{
    Mat hsvImg;
    cvtColor(img, hsvImg, CV_BGR2HSV);
    vector<Mat> hsv_planes;
    split(hsvImg, hsv_planes);
    float maxValue = 255;
    float saturationTotal = 0;
    float valueTotal = 0;
    float totalPixels = 0;

    // Acrescimo na altura da cabeça para se chegar ao ombro
    float delta_cabeca = 0.15f;

    // Limite da cabeça
    int x, w, y, h;

    //   int xcentro = img_.cols / 2;
    if (!faceEstimada) {
        x = face.x - 0.4*delta_cabeca * face.width;
        if (x < 0)  x = 1;

        w = x + face.width + 0.8 * delta_cabeca * face.width;
        if (w > img.cols)  w = img.cols - 1;

        y = face.y - 1.5*delta_cabeca*face.height;          // Linha inicial
        if (y < 0) y = 1;

        h = y + face.height + 3 * delta_cabeca*face.height; // Linha final
        if (h>img.rows) h = img.rows - 1;
    } else {
        x = face.x;
        w = x + face.width;
        y = face.y;
        h = y + face.height;
    }
    Mat copy = img.clone();
    for (int i = 0; i < img.rows; i++)
    {
        for (int j = 0; j < img.cols; j++)
        {
            if ((j>x && j<w) && (i>y && i<h))
            {
                Vec3b valPixel;
                //Debug
                valPixel[0] = 0;   //B
                valPixel[1] = 255;   //G
                valPixel[2] = 0; //R
                copy.at<Vec3b>(i, j) = valPixel;
            } else if (i >= h) {
                Vec3b valPixel;
                //Debug
                valPixel[0] = 0;   //B
                valPixel[1] = 255;   //G
                valPixel[2] = 0; //R
                copy.at<Vec3b>(i, j) = valPixel;
            } else if (i < h) {
                totalPixels++;
                Vec3b hsv = hsvImg.at<Vec3b>(i, j);
                saturationTotal += hsv.val[1];
                valueTotal += hsv.val[2];
            }
        }
    }
    float saturationPercentage = 100 * (saturationTotal / totalPixels) / maxValue;
    float valuePercentage = 100 * (valueTotal / totalPixels) / maxValue;
    return (saturationPercentage <= maxSaturation && valuePercentage >= minSaturation);
}
