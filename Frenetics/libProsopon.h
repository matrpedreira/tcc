#ifndef LIBPROSOPON_HPP
#define LIBPROSOPON_HPP

#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class libProsopon
{

public:

    // Constructor
    libProsopon();

    // Destructor
    ~libProsopon();

    // FunÁ„o que verifica se o arquivo aberto È uma imagem.
    bool funcVerificaImg(Mat);

    // FunÁ„o que pega a altura e a largura da imagem e verifica se a quantidade de linhas e colunas est· acima de um determinado limiar.
    bool funcDimensoes(const Mat, int);

    // Verifica se a imagem È  colorida
    bool funcColor(const Mat);

    // FunÁ„o que pega a altura e a largura da imagem e verifica se a raz„o entre essas duas medidas est· adequada (formato retrato).
    bool funcRazao(const Mat, float, float);

    // FunÁ„o que verifica a raz„o entre a altura da face e a altura da imagem.
    int funcAlturaRelat(Mat, cv::Rect, float, float);

    // FunÁ„o que verifica se o tamanho em bytes da imagem est· adequado.
    bool funcTamanhoBytes(Mat, long double, long double, long double);

    // FunÁ„o que detecta faces na imagem.
    bool funcFaces(Mat, vector<cv::Rect> &, CascadeClassifier);

    // FunÁ„o que detecta olhos na imagem onde h· face
    bool funcOlhosComFace(Mat, cv::Rect, vector<cv::Rect> &, CascadeClassifier);

    // FunÁ„o que detecta olhos na imagem sem face
    bool funcOlhosSemFace(Mat, vector<cv::Rect> &Olhos, CascadeClassifier);

    bool funcEyesDistance(Mat img, vector<cv::Rect> &Olhos, vector<cv::Rect> &eyeDistance, int maximumDistance);

    bool eyesHeightValid(Mat img, vector<cv::Rect> &Olhos, int minimumHeight, int maximumHeight) ;

    bool funcEyesAlignment(Mat img, vector<cv::Rect> &eyeDistance, vector<cv::Rect> &Olhos);

    // Verifica se o fundo È claro.
    bool funcFundoClaro(Mat , cv::Rect , float , float , bool);

    bool eyesToTopValid(Mat img, vector<cv::Rect> &Olhos);

    // FunÁ„o que calcula o tamanho da imagem em bytes.
    long double funcCalcBytes(Mat img);

    // Estima os limites da cabeÁa a partir dos olhos.
    cv::Rect funcEstimaHead(Mat, vector<cv::Rect>);

    // Verifica se o fundo È ruidoso.
    bool funcFundo(Mat, cv::Rect, float, float, Mat &);

    // Verifica se a cabeÁa est· centralizada.
    bool funcCentro(const Mat &, cv::Rect, float);

    // Converte matriz em imagem gray.
    Mat mat2gray(const Mat&);

    // FunÁ„o que desenha uma linha do centro da imagem para o centro da face.
    void funcRetaCentro(Mat&, cv::Rect, float);

    // Verifica se a imagem est· borrada ou n„o.
    bool funcVarOfLapl(const Mat& img, float);

    // Verifica o contraste entre o fundo e a face.
    bool funcContraste(Mat, cv::Rect ,float );


    
private:
    
};



#endif

