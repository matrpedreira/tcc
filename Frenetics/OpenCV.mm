//
//  OpenCV.m
//  OpenCVSample_iOS
//
//  Created by Hiroki Ishiura on 2015/08/12.
//  Copyright (c) 2015年 Hiroki Ishiura. All rights reserved.
//

// Put OpenCV include files at the top. Otherwise an error happens.

/// Converts an UIImage to Mat.
/// Orientation of UIImage will be lost.
#import "OpenCV.h"
#import <Foundation/Foundation.h>
#import "libProsopon.h"

#define MEDIAN_MATRIX 7
#define DILATATION_SIZE 6
#define THRESHOLD_VALUE 10
#define BILATERAL_FILTER 9
#define QUANTITIZE_FACTOR 24

static void UIImageToMat(UIImage *image, cv::Mat &mat) {
	
	// Create a pixel buffer.
	NSInteger width = CGImageGetWidth(image.CGImage);
	NSInteger height = CGImageGetHeight(image.CGImage);
	CGImageRef imageRef = image.CGImage;
	cv::Mat mat8uc4 = cv::Mat((int)height, (int)width, CV_8UC4);
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGContextRef contextRef = CGBitmapContextCreate(mat8uc4.data, mat8uc4.cols, mat8uc4.rows, 8, mat8uc4.step, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrderDefault);
	CGContextDrawImage(contextRef, CGRectMake(0, 0, width, height), imageRef);
	CGContextRelease(contextRef);
	CGColorSpaceRelease(colorSpace);
	
	// Draw all pixels to the buffer.
	cv::Mat mat8uc3 = cv::Mat((int)width, (int)height, CV_8UC3);
	cv::cvtColor(mat8uc4, mat8uc3, CV_RGBA2BGR);
	
	mat = mat8uc3;
}

/// Converts a Mat to UIImage.
static UIImage *MatToUIImage(cv::Mat &mat) {
	
	// Create a pixel buffer.
	assert(mat.elemSize() == 1 || mat.elemSize() == 3);
	cv::Mat matrgb;
	if (mat.elemSize() == 1) {
		cv::cvtColor(mat, matrgb, CV_GRAY2RGB);
	} else if (mat.elemSize() == 3) {
		cv::cvtColor(mat, matrgb, CV_BGR2RGB);
	}
	
	// Change a image format.
	NSData *data = [NSData dataWithBytes:matrgb.data length:(matrgb.elemSize() * matrgb.total())];
	CGColorSpaceRef colorSpace;
	if (matrgb.elemSize() == 1) {
		colorSpace = CGColorSpaceCreateDeviceGray();
	} else {
		colorSpace = CGColorSpaceCreateDeviceRGB();
	}
	CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
	CGImageRef imageRef = CGImageCreate(matrgb.cols, matrgb.rows, 8, 8 * matrgb.elemSize(), matrgb.step.p[0], colorSpace, kCGImageAlphaNone|kCGBitmapByteOrderDefault, provider, NULL, false, kCGRenderingIntentDefault);
	UIImage *image = [UIImage imageWithCGImage:imageRef];
	CGImageRelease(imageRef);
	CGDataProviderRelease(provider);
	CGColorSpaceRelease(colorSpace);
	
	return image;
}

#pragma mark -

@implementation OpenCV

bool finishedEdges = false;
bool finishedColors = false;
cv::Mat edgesMat;
cv::Mat colorsMat;
cv::Mat bgrMat;
int pipelineCounter = 0;
CascadeClassifier _faceDetector;
CascadeClassifier _eyeDetector;

bool faceDetected = false;
bool eyesDetected = false;
int faceRatioCorrect = -1;
bool eyeDistanceValid = false;
bool eyeAlignmentValid = false;
bool eyeHeightValid = false;
bool eyeDistanceTopValid = false;
bool faceCenterCorrect = false;
bool backgroundClearValid = false;
bool backgroundNoiseValid = false;
bool backgroundContrasteValid = false;
bool faceEstimada = false;

vector<cv::Rect> estimatedHeads;
vector<cv::Rect> eyesPairs;
vector<cv::Rect> eyesDistance;
cv::Rect faceLocation;

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"haarcascade_frontalface_default" ofType:@"xml"];
        const char *filePath = [path cStringUsingEncoding:NSUTF8StringEncoding];
        _faceDetector = CascadeClassifier(filePath);
        NSString *pathEye = [[NSBundle mainBundle] pathForResource:@"haarcascade_eye" ofType:@"xml"];
        const char *filePathEye = [pathEye cStringUsingEncoding:NSUTF8StringEncoding];
        _eyeDetector = CascadeClassifier(filePathEye);
        eyesDistance = vector<cv::Rect>();
    }
    return self;
}

- (void) cleanVariables {
    faceDetected = false;
    eyesDetected = false;
    faceRatioCorrect = -1;
    eyeDistanceValid = false;
    eyeAlignmentValid = false;
    eyeHeightValid = false;
    eyeDistanceTopValid = false;
    faceCenterCorrect = false;
    backgroundClearValid = false;
    backgroundNoiseValid = false;
    backgroundContrasteValid = false;
    faceEstimada = false;
}

- (NSDictionary*)faceRecognition:(cv::Mat &)image withTag:(int)tag
{

    libProsopon prosoponLib = libProsopon();
    int height = floor(image.rows / 45) * 45;
    int width = floor(image.cols / 35) * 35;
    NSLog(@"height: %d, width:%d",height,width);
    int initialY = (image.rows - height)/2;
    int initialX = (image.cols - width)/2;

    cv::Rect myROI(initialX, initialY, width, height);
    cv::Mat croppedImage = image(myROI);

    cv:Mat testImage;
    cvtColor(croppedImage, testImage, CV_BGRA2BGR);

    if (tag != 4) {
        switch (pipelineCounter) {
            case 0:
                faceDetected = prosoponLib.funcFaces(testImage, estimatedHeads, _faceDetector);
                break;
            case 1:
                if (faceDetected) {
                    eyesDetected = prosoponLib.funcOlhosComFace(testImage, estimatedHeads.front(), eyesPairs, _eyeDetector);
                } else {
                    eyesDetected = prosoponLib.funcOlhosSemFace(testImage, eyesPairs, _eyeDetector);
                }
                break;
            case 2:
                if (!faceDetected && eyesDetected) {
                    estimatedHeads.push_back(prosoponLib.funcEstimaHead(testImage, eyesPairs));
                    faceDetected = true;
                    faceEstimada = true;
                }
                break;
            case 3:
                if (tag == 2) {
                    faceRatioCorrect = 0;
                } else {
                    if (faceDetected) {
                        faceRatioCorrect = prosoponLib.funcAlturaRelat(testImage, estimatedHeads.front(), 50, 70);
                    }
                }
                break;
            case 4:
                if (eyesDetected) {
                    eyeDistanceValid = prosoponLib.funcEyesDistance(testImage, eyesPairs, eyesDistance, testImage.cols/2);
                } else {
                    eyesDistance.clear();
                    eyeDistanceValid = false;
                }
                break;
            case 5:
                if (eyesDetected) {
                    eyeDistanceTopValid = prosoponLib.eyesToTopValid(testImage, eyesPairs);
                } else {
                    eyeDistanceTopValid = false;
                }
                break;
            case 6:
                if (tag == 1) {
                    eyeAlignmentValid = true;
                } else {
                    if (eyeDistanceValid) {
                        eyeAlignmentValid = prosoponLib.funcEyesAlignment(testImage, eyesDistance, eyesPairs);
                    } else {
                        eyeAlignmentValid = true;
                    }
                }
                break;
            case 7:
                if (tag == 1) {
                    faceCenterCorrect = true;
                }else {
                    if (faceDetected) {
                        faceCenterCorrect = prosoponLib.funcCentro(testImage, estimatedHeads.front(),10);
                    } else {
                        faceCenterCorrect = false;
                    }
                }
            case 8:
                if (tag == 3 || tag == 1) {
                    backgroundClearValid = true;
                } else {
                    if (faceDetected) {
                        backgroundClearValid = prosoponLib.funcFundoClaro(testImage, estimatedHeads.front(), 10.0, 90.0,faceEstimada);
                    } else {
                        backgroundClearValid = false;
                    }
                }
            case 9:
                if (tag == 3 || tag == 1) {
                    backgroundNoiseValid = true;
                } else {
                    if (faceDetected) {
                        cv::Mat newMat;
                        backgroundNoiseValid = prosoponLib.funcFundo(testImage, estimatedHeads.front(), 30, 10, newMat);
                    } else {
                        backgroundNoiseValid = false;
                    }
                }
            default:
                if (pipelineCounter == 10) {
                    pipelineCounter = -1;
                }
                break;
        }
        pipelineCounter++;
    } else {
        faceDetected = true;
        eyesDetected = true;
        faceRatioCorrect = 0;
        eyeDistanceValid = true;
        eyeAlignmentValid = true;
        eyeHeightValid = true;
        eyeDistanceTopValid = true;
        faceCenterCorrect = true;
        backgroundClearValid = true;
        backgroundNoiseValid = true;
        backgroundContrasteValid = true;
    }
    return @{@"faceDetected": [[NSNumber alloc] initWithBool:faceDetected],
             @"eyesDetected":[[NSNumber alloc] initWithBool:eyesDetected],
             @"faceRationCorrect":[[NSNumber alloc] initWithInt:faceRatioCorrect],
             @"eyesDistanceCorrect":[[NSNumber alloc] initWithBool:eyeDistanceValid],
             @"eyesDistanceTopCorrect":[[NSNumber alloc] initWithBool:eyeDistanceTopValid],
             @"eyesAlignmentCorrect":[[NSNumber alloc] initWithBool:eyeAlignmentValid],
             @"backgroundColorCorrect":[[NSNumber alloc] initWithBool:backgroundClearValid],
             @"backgroundNoiseValid":[[NSNumber alloc] initWithBool:backgroundNoiseValid],
             @"faceCenterCorrect":[[NSNumber alloc] initWithBool:faceCenterCorrect]};
}


+ (void)cartonEffect:(UIImage *)image withView:(UIViewController*)view{
	UIImageToMat(image, bgrMat);
    dispatch_queue_t myCustomQueue = dispatch_queue_create("Edges", NULL);
    dispatch_async( myCustomQueue, ^{
        edgesMat = [OpenCV findEdges:bgrMat];
        finishedEdges = true;
        if(finishedColors){
            dispatch_async(dispatch_get_main_queue(), ^{
                cv::Mat result = [OpenCV finish];
                [view performSelector:@selector(setImage:) withObject:MatToUIImage(result)];
            });
        }
    });
    dispatch_queue_t myCustomQueue2 = dispatch_queue_create("Colors", NULL);;
    dispatch_async(myCustomQueue2, ^{
        colorsMat = [OpenCV getColors:bgrMat];
        finishedColors = true;
        if(finishedEdges){
            dispatch_async(dispatch_get_main_queue(), ^{
                cv::Mat result = [OpenCV finish];
                [view performSelector:@selector(setImage:) withObject:MatToUIImage(result)];
            });
        }
    });
}

+ (void)cartonMatEffect:(cv::Mat &)image withView:(UIViewController*)view{
    cv::Mat initImage;
    cvtColor(image, initImage, CV_BGRA2BGR);
    edgesMat = [OpenCV findEdges:initImage];
    finishedEdges = true;
    colorsMat = [OpenCV getColors:initImage];
    cv::Mat result = [OpenCV finish];
    cvtColor(result, image, CV_BGR2BGRA);
}


//Finding edges

+(cv::Mat) finish{
    cv::Mat edgesFinal;
    cv::cvtColor(edgesMat, edgesFinal, CV_GRAY2BGR);
    cv::Mat result = colorsMat;
    for(int i=0; i<result.rows;i++) {
        for(int j=0;j<result.cols;j++){
            result.at<cv::Vec3b>(i,j) = result.at<cv::Vec3b>(i,j).mul(edgesFinal.at<cv::Vec3b>(i,j));
        }
    }
    //    cv::Point2f pt(result.cols/2,result.rows/2);
////    cv::Mat rotationMat = cv::getRotationMatrix2D(pt, 180.0, 1.0);
//    cv::warpAffine(result, result, rotationMat, cv::Size(result.cols,result.rows));
    std::string type = [OpenCV type2str:result.type()];
    return result;
}

+ (std::string) type2str:(int)type{
    std::string r;
    
    uchar depth = type & CV_MAT_DEPTH_MASK;
    uchar chans = 1 + (type >> CV_CN_SHIFT);
    
    switch ( depth ) {
        case CV_8U:  r = "8U"; break;
        case CV_8S:  r = "8S"; break;
        case CV_16U: r = "16U"; break;
        case CV_16S: r = "16S"; break;
        case CV_32S: r = "32S"; break;
        case CV_32F: r = "32F"; break;
        case CV_64F: r = "64F"; break;
        default:     r = "User"; break;
    }
    
    r += "C";
    r += (chans+'0');
    
    return r;
}

+ (cv::Mat)findEdges:(cv::Mat)imageMat{
    cv::Mat medianMat = [self medianFilter:imageMat];
    cv::Mat cannyMat = [self cannyEdgeDetector:medianMat];
    cv::Mat dilateMat = [self dilatation:cannyMat];
    cv::Mat treshold = [self threshold:dilateMat];
    return treshold;
}

+ (cv::Mat) medianFilter:(cv::Mat)imageMat{
    cv::Mat medianMat;
    cv::blur(imageMat, medianMat, cv::Size(MEDIAN_MATRIX,MEDIAN_MATRIX));
    return medianMat;
}

+ (cv::Mat)dilatation:(cv::Mat)imageMat{
    cv::Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(DILATATION_SIZE,DILATATION_SIZE));
    cv::Mat result;
    cv::dilate(imageMat, result, element);
    return result;
}

+ (cv::Mat)threshold:(cv::Mat)imageMat{
    cv::Mat result;
    cv::threshold(imageMat, result, THRESHOLD_VALUE, 1, 1);
    return result;
}

+ (cv::Mat) cannyEdgeDetector:(cv::Mat)imageMat{
    cv::Mat cannyMat;
    cv::Canny(imageMat, cannyMat, 20, 100);
    cv::Mat dst;
    dst = cv::Scalar::all(0);
    cannyMat.copyTo(dst);
    return dst;
}

//Playing with color
+ (cv::Mat)getColors:(cv::Mat)imageMat{
    cv::Mat pyrDownMat;
    cv::pyrDown(imageMat, pyrDownMat);
    cv::pyrDown(pyrDownMat, pyrDownMat);
    cv::Mat bilateralMat = [self bilateralFilter:pyrDownMat];
    cv::Mat medianMat = [self medianFilter:bilateralMat];
    cv::Mat result = [self quantitizeColor:medianMat];
    cv::pyrUp(result, result);
    cv::pyrUp(result, result);
    return result;
}

+ (cv::Mat)bilateralFilter:(cv::Mat)imageMat{
    cv::Mat result = imageMat;
    for(int i=0; i<14;i++){
        cv::Mat nextBilateral;
        cv::bilateralFilter(result, nextBilateral, BILATERAL_FILTER, BILATERAL_FILTER*2, BILATERAL_FILTER/2);
        result = nextBilateral;
    }
    return result;
}

+ (cv::Mat)quantitizeColor:(cv::Mat)imageMat{
    cv::Mat result = imageMat;
    for(int colors=0;colors<2;colors++){
        for(int i=0;i<result.rows;i++){
            for(int j=0;j<result.cols;j++){
                result.at<cv::Vec3b>(i,j)[colors] = floor(result.at<cv::Vec3b>(i,j)[colors]/QUANTITIZE_FACTOR)*QUANTITIZE_FACTOR;
            }
        }
    }
    return result;
}

@end
