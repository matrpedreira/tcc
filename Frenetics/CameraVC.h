//
//  CameraVC.h
//  Frenetics
//
//  Created by Matheus Pedreira on 5/9/16.
//  Copyright © 2016 UnB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CvPhotoCameraMod.h"

using namespace cv;


@interface CameraVC : UIViewController
{
    CvPhotoCameraMod* videoCamera;
    __weak IBOutlet UIImageView *mainCameraView;
}

@property (nonatomic, retain) CvPhotoCameraMod* videoCamera;
@property (nonatomic, retain) UIImageView *mainCameraView;
@end
