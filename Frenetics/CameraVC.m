//
//  CameraVC.m
//  Frenetics
//
//  Created by Matheus Pedreira on 5/9/16.
//  Copyright © 2016 UnB. All rights reserved.
//

#import "CameraVC.h"
#import "OpenCV.h"
#import "ResultViewController.h"


@interface CameraVC () <CvPhotoCameraDelegateMod>


@property (weak, nonatomic) IBOutlet UILabel *message;

@property OpenCV* openCV;
@end

@implementation CameraVC



-(void)viewDidLoad
{
    _videoCamera = [[CvPhotoCameraMod alloc] initWithParentView:_mainCameraView];
    _videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
    _videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPresetHigh;
    _videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    _videoCamera.defaultFPS = 24;
    _videoCamera.delegate = self;
    [_videoCamera createCustomVideoPreview];
    _openCV = [[OpenCV alloc]init];
    [_videoCamera start];
//    self.navigationController.navigationBar.hidden = true;

}

-(void)viewWillAppear:(BOOL)animated {
    [_openCV cleanVariables];
    [_videoCamera start];
}

- (IBAction)changeCamera:(id)sender
{
    [_videoCamera switchCameras];
}

#pragma mark - Protocol CvVideoCameraDelegate

#ifdef __cplusplus
- (void)processImage:(Mat&)image;
{
    NSDictionary* results = [_openCV faceRecognition:image withTag:self.view.tag];
        dispatch_async(dispatch_get_main_queue(), ^{
        if (![((NSNumber*)results[@"faceDetected"]) boolValue]) {
            [_message setText:@"Nenhum rosto detectado"];
        } else if (![((NSNumber*)results[@"eyesDetected"]) boolValue]) {
            [_message setText:@"Nenhum olho detectado"];
        } else if ([((NSNumber*)results[@"faceRationCorrect"]) intValue] != 0) {
            int value = [((NSNumber*)results[@"faceRationCorrect"]) intValue];
            if (value == -1) {
                [_message setText:@"Chegue mais perto da câmera"];
            } else if (value == 1) {
                [_message setText:@"Se afaste mais da câmera"];
            }
        } else if (![((NSNumber*)results[@"eyesDistanceCorrect"]) boolValue]) {
            [_message setText:@"Se afaste mais da câmera"];
        } else if (![((NSNumber*)results[@"eyesDistanceTopCorrect"]) boolValue]) {
            [_message setText:@"Mantenha seus olhos acima da linha horizontal"];
        } else if (![((NSNumber*)results[@"eyesAlignmentCorrect"]) boolValue]) {
            [_message setText:@"Deixe seus olhos alinhados horizontalmente"];
        } else if (![((NSNumber*)results[@"faceCenterCorrect"]) boolValue]) {
            [_message setText:@"Centralize a sua face"];
        } else if (![((NSNumber*)results[@"backgroundColorCorrect"]) boolValue]) {
            [_message setText:@"O fundo deve ser claro"];
        } else if (![((NSNumber*)results[@"backgroundNoiseValid"]) boolValue]) {
            [_message setText:@"Background noise not correct"];
        } else {
            [_message setText:@""];
            [_videoCamera takePicture];
            [_videoCamera stop];
        }
    });
    // Do some OpenCV stuff with the image
}

- (void)photoCamera:(CvPhotoCamera *)photoCamera capturedImage:(UIImage *)image {
    ResultViewController* vc = [[UIStoryboard storyboardWithName:@"ResultImage" bundle:nil] instantiateInitialViewController];
    vc.inputImage = image;
    [self.navigationController pushViewController:vc animated:true];
}




#endif



@end
