//
//  OpenCV.h
//  OpenCVSample_iOS
//
//  Created by Hiroki Ishiura on 2015/08/12.
//  Copyright (c) 2015年 Hiroki Ishiura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <opencv2/opencv.hpp>
#import <opencv2/imgproc.hpp>
#import <opencv2/videoio/cap_ios.h>
#import <cmath>
#import <vector>

@interface OpenCV : NSObject

/// Converts a full color image to grayscale image with using OpenCV.
+ (void)cartonEffect:(UIImage *)image withView:(UIViewController*)view;
+ (void)cartonMatEffect:(cv::Mat&)image withView:(UIViewController*)view;
- (NSDictionary*)faceRecognition:(cv::Mat &)image withTag:(int)tag;
- (void) cleanVariables;

@end
