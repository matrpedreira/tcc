//
//  ResultViewController.h
//  Frenetics
//
//  Created by Matheus Pedreira on 9/25/16.
//  Copyright © 2016 UnB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewController : UIViewController

@property (nonatomic, retain) UIImage* inputImage;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
