//
//  ResultViewController.m
//  Frenetics
//
//  Created by Matheus Pedreira on 9/25/16.
//  Copyright © 2016 UnB. All rights reserved.
//

#import "ResultViewController.h"

@implementation UIImage (Crop)

- (UIImage *)crop:(CGRect)rect {

    //transform visible rect to image orientation
    CGAffineTransform rectTransform = [self orientationTransformedRectOfImage:self];
    rect = CGRectApplyAffineTransform(rect, rectTransform);

    //crop image
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef scale:self.scale orientation:self.imageOrientation];
    CGImageRelease(imageRef);
    return result;
}

#define rad(angle) ((angle) / 180.0 * M_PI)
- (CGAffineTransform)orientationTransformedRectOfImage:(UIImage *)img
{
    CGAffineTransform rectTransform;
    switch (img.imageOrientation)
    {
        case UIImageOrientationLeft:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(rad(90)), 0, -img.size.height);
            break;
        case UIImageOrientationRight:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(rad(-90)), -img.size.width, 0);
            break;
        case UIImageOrientationDown:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(rad(-180)), -img.size.width, -img.size.height);
            break;
        default:
            rectTransform = CGAffineTransformIdentity;
    };

    return CGAffineTransformScale(rectTransform, img.scale, img.scale);
}

@end

@interface ResultViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBorder;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *aspectWidth;

@end

@implementation ResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    int initialHeight = _inputImage.size.height;
    int initialWidth = _inputImage.size.width;

    int height = floor(_inputImage.size.height / 45) * 45;

    int width = floor(_inputImage.size.width / 35) * 35;
    int initialY = (_inputImage.size.height - height)/2;
    int initialX = (_inputImage.size.width - width)/2;
    NSString* text = [NSString stringWithFormat:@"Initial Height: %f Initial Width: %f",_inputImage.size.height,_inputImage.size.width];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Medidas iniciais" message:text delegate:self cancelButtonTitle:@"Hide" otherButtonTitles:nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];

    _aspectWidth.constant = (initialWidth*initialX)/_imageView.frame.size.width;
    _topBorder.constant = (initialHeight*initialY)/_imageView.frame.size.height;




    [_imageView setImage:_inputImage];

    self.navigationController.navigationBar.hidden = false;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)saveImage {
    int height = floor(_inputImage.size.height/45) * 45;
    int width = floor(_inputImage.size.width / 35) * 35;
    int initialY = (_inputImage.size.height - height)/2;
    int initialX = (_inputImage.size.width - width)/2;
    NSString* text = [NSString stringWithFormat:@"Height: %d Width: %d X: %d Y: %d",height,width,initialX,initialY];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Medidas finais" message:text delegate:self cancelButtonTitle:@"Hide" otherButtonTitles:nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
    CGRect cropRect = CGRectMake(initialX, initialY, width, height);
    UIImage *uiImage = [_inputImage crop:cropRect];
    UIImageWriteToSavedPhotosAlbum(uiImage,nil,nil,nil);
}

- (IBAction)saveImage:(id)sender {
    [self saveImage];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
