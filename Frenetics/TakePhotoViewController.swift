////
////  TakePhotoViewController.swift
////  Frenetics
////
////  Created by Matheus Pedreira on 3/2/16.
////  Copyright © 2016 UnB. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//class TakePhotoViewController:UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
//    
//    @IBOutlet weak var mImageView: UIImageView!
//    
//    @IBOutlet weak var mApplyFilter: UIButton!
//    
////    override func viewDidLoad() {
////        <#code#>
////    }
////    override func viewWillAppear(animated: Bool) {
////        <#code#>
////    }
//    
//    @IBAction func takePhoto(sender: AnyObject) {
//        let picker = UIImagePickerController();
//        picker.delegate = self;
//        picker.allowsEditing = false;
//        let alertController = UIAlertController(title: "Select picture", message: "", preferredStyle: .Alert)
//        let cameraAction = UIAlertAction(title: "Camera", style: .Default) { (action) in
//            picker.sourceType = UIImagePickerControllerSourceType.Camera;
//            self.presentViewController(picker, animated: true, completion: nil)
//        }
//        let galeryAction = UIAlertAction(title: "Galery", style: .Default) { (action) in
//           picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
//           self.presentViewController(picker, animated: true, completion: nil)
//        }
//        let cancelAction = UIAlertAction(title: "Cancel", style: .Destructive, handler:nil)
//        alertController.addAction(cameraAction)
//        alertController.addAction(galeryAction)
//        alertController.addAction(cancelAction)
//        self.presentViewController(alertController, animated: true, completion: nil)
//    }
//    
//    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
//        mImageView.image = image;
//        picker.dismissViewControllerAnimated(true, completion: nil)
//    }
//    
//    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
//        
//        picker.dismissViewControllerAnimated(true, completion: nil);
//    }
//    
//    @IBAction func applyFilter(sender: AnyObject) {
//        OpenCV.cartonEffect(mImageView.image,withView: self);
//    }
//    
//    func setImage(image:UIImage){
//        mImageView.image = image;
//    }
//    
//    
//}
//
//
